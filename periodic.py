import json
import hmac
import hashlib
import requests
from types import *

class Provider:

  def __init__(self, api):
    self.api = api
    if self.api.__class__.__name__ != 'PeriodicGateway':
      raise Exception("API must be of type PeriodicAPI")

class PeriodicGateway:

  def __init__(self, apikey, endpoint_base='periodicapp.com'):
    self.setApikey(apikey)
    self.endpointbase = endpoint_base

  def __repr__(self):
    return "PERIODIC!"

  def getEndpoint(self, subdomain=None):
    if subdomain:
      return "http://{subdomain}.{endpoint}".format(subdomain=subdomain, endpoint=self.endpointbase)
    else:
      return "http://{endpoint}".format(endpoint=self.endpointbase)

  def setApikey(self, apikey):
    self.apikey = apikey

  def serialize(self, obj):
    if type(obj) == DictType:
      return ''.join(["{k}{v}".format(k=k, v=self.serialize(v)) for k,v in sorted(obj.items())])
    else:
      return "{obj}".format(obj=obj)

  def digest(self, obj):
    serialization = self.serialize(obj)
    return hmac.new(self.apikey, msg=serialization, digestmod=hashlib.md5).hexdigest()

  def testDigest(self, obj):
    digest = self.digest(obj)
    print(digest)
    auth_header = "{username}::{digest}".format(username=self.apikey, digest=digest)
    headers = {'Authorization': auth_header, 'Content-type': 'application/json'}
    url = self.getEndpoint() + "/apikey"
    r = requests.post(url, data=json.dumps(obj), headers=headers)
    return r.text == 'true'

  def get(self, endpoint):
    pass

  def delete(self, endpoint, entityid):
    pass

  def post(self, endpoint, request_body):
    pass

  def put(self, endpoint, request_body):
    pass

